// 'gulp' for build
// 'gulp serve' for livereload

const gulp = require('gulp');
const gulpif = require('gulp-if');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const stylus = require('gulp-stylus')
const autoprefixer = require('gulp-autoprefixer')
const browserSync = require('browser-sync').create();
const emitty = require('emitty').setup('src/templates', 'pug', {
  makeVinylFile: true
});

const folder = {
  src :'src/',
  dist:'dist/'
};

const path = {
  pug: 'templates/**/*.pug',
  sass: 'sass/**/*.scss',
  fonts: 'fonts/*.*',
  images: 'img/**/*.*',
  js: 'js/**/*.*'
};

const pathMkdirs = {
  css: folder.dist + 'css/',
  fonts: folder.dist + 'fonts/',
  images: folder.dist + 'images/',
  js: folder.dist + 'js/'
};

gulp.task('mkdirs', function () {
  return gulp.src('*.*', {read: false})
      .pipe(gulp.dest(pathMkdirs.css))
      .pipe(gulp.dest(pathMkdirs.fonts))
      .pipe(gulp.dest(pathMkdirs.images))
      .pipe(gulp.dest(pathMkdirs.js));
});


gulp.task('html',function(){
  return gulp.src(folder.src + path.pug)
  .pipe(pug({
    pretty: true
  }))
  .pipe(gulp.dest(folder.dist))
  .pipe(browserSync.stream());
});

gulp.task('css',function(){
  return gulp.src(folder.src + path.sass)
  .pipe(sass())
  .pipe(autoprefixer())
  .pipe(gulp.dest(folder.dist + 'css/'))
  .pipe(browserSync.stream());
});

gulp.task('js',function(){
  return gulp.src(folder.src + path.js)
  .pipe(gulp.dest(folder.dist + 'js'))
  .pipe(browserSync.stream());
});

gulp.task('fonts',function(){
  return gulp.src(folder.src + path.fonts)
  .pipe(gulp.dest(folder.dist + 'fonts'))
  .pipe(browserSync.stream());
});

gulp.task('images',function(){
  return gulp.src(folder.src + path.images)
  .pipe(gulp.dest(folder.dist + 'images'))
  .pipe(browserSync.stream());
});


gulp.task('serve', function() { 
  browserSync.init({
    open: false,
    server: {
      baseDir: "./dist",
      //online: false
      browser: "google chrome",
      
      reloadDelay: 1000
    },
  });


  gulp.watch(folder.src + path.sass, ['css']);
  gulp.watch(folder.src + path.pug, ['html']);
  gulp.watch(folder.src + path.js, ['js']);

  // dont forget to place body tag in html (localhost:3001/help)
});






gulp.task('default',['mkdirs','html','css','js','images','fonts']);

