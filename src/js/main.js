window.onload = function () {

    $('.modal-window .button').click(function () {
        $('.modal-overlay').hide()
	});
		
    $('.modal_trigger').click(function (e) {
        e.preventDefault();
        let thisHref = $(this).attr('href');
        $(thisHref).addClass('modal_show');
        $('.section-modals').addClass('modal_overlay');
        
        $('body').addClass('overflow_hidden');
        
	})
		
		$('.modal__can').click(function(){
			$(this).parent().parent().removeClass('modal_show');
            $('.section-modals').removeClass('modal_overlay');
            
            $('body').removeClass('overflow_hidden');
            
        });
        
        $('.modal_firstauth .button_y').click(function(){
            $('.modal#firstauth').removeClass('modal_show');
        });
        $('.modal_firstauth .button_n').click(function(){
            $('.modal#firstauth').removeClass('modal_show');
        });

        $('.modal_auth .modal_trigger').click(function(){
            $('.modal#auth').removeClass('modal_show');
        });

//input mask
    (function () {
        $("input[type='tel']").inputmask({
            mask:"+7(999)-999-99-99",
            "clearIncomplete": true
        });
        //$('.field_table_cal input').mask('99 - 99 - 99');
        $(".modal_getgift input[placeholder='ДД/ММ/ГГ' ]").inputmask('99/99/99');
    })();

    
    //tabs
    (function () {
        $(".tab_wrapper .tab").removeClass("active");
        $(".tab_item").not(":first").hide();
        $(".tab_wrapper .tab").click(function () {
            $(".tab_wrapper .tab").removeClass("active").eq($(this).index()).addClass("active");
            $(".tab_item").hide().eq($(this).index()).fadeIn()
        }).eq(0).addClass("active");
    })();
    

    //up to top
    (function () {
        if ($('.up').length) {
            var scrollTrigger = 1000, // px
            backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('.up').addClass('up_showed');
                    } else {
                        $('.up').removeClass('up_showed');
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('.up').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }
    })();

    $('.menu').click(function(e){
        $(this).toggleClass('menu_mobile_active');
        if($(window).width() <= 1100){

            $('body').toggleClass('overflow_hidden');
        }
        //e.stopPropagation();
        $('.header_container').toggleClass('header_bg_anim');
        $('.header__inner').toggleClass('header_inner_anim');
        $('.nav').toggleClass('header_nav_anim')

    })

    $('.js-example-basic-single').select2({
        "language": {
            "noResults": function(){
                return "Совпадений не найдено";
            }
        }
    });
    
    // $.validator.addMethod("checkMask", function(value, element) {
    //     return /\+\d{1}\(\d{3}\)\d{3}-\d{4}/g.test(value); 
    // });

    $("#auth form").validate({
        rules: {
            'phone': {
              required: true,
              //checkMask: true
            },
            'pass': {
              required: true
            },
        },
        messages: {
            phone: 'Заполните это поле',
            pass: 'Заполните это поле'
        },
        errorElement : 'span',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          var label = $(element).parent();
          if (placement) {
            $(label).append(error)
          } else {
            error.insertBefore(label);
          }
        }
    });

    $("#reg form").validate({
        rules: {
            'surname': {
                required: true,
            },
            'name': {
                required: true
            },
            'phone': {
                required: true,
              },
            'mail': {
                required: true
            },
            'rules': {
                required: true
              },
            'privacy': {
                required: true,
            },
            'captha': {
                required: true
            },
            
        },
        messages: {
            surname: 'Заполните это поле',
            name: 'Заполните это поле',
            phone: 'Заполните это поле',
            mail: 'Заполните это поле',
            rules: 'Потвердите согласие',
            privacy: 'Потвердите согласие',
            captha: 'Потвердите что вы не робот'
        },
        errorElement : 'span',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          var label = $(element).parent();
          if (placement) {
            $(label).append(error)
          } else {
            error.insertBefore(label);
          }
        }
    });

    $("#getgift form").validate({
        rules: {
            'bdate': {
                required: true,
            },
            'adress': {
                required: true
            },
            'adressdel': {
                required: true,
              },
            'city': {
                required: true
            },
            'st': {
                required: true
              },
            'housenum': {
                required: true,
            },
            
        },
        messages: {
            bdate: 'Заполните это поле',
            adress: 'Заполните это поле',
            adressdel: 'Заполните это поле',
            city: 'Заполните это поле',
            st: 'Заполните это поле',
            housenum: 'Заполните это поле',
        },
        errorElement : 'span',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          var label = $(element).parent();
          if (placement) {
            $(label).append(error)
          } else {
            error.insertBefore(label);
          }
        }
    });

    $("#questions form").validate({
        rules: {
            'email': {
                required: true,
            },
            'text': {
                required: true,
            },
        },
        messages: {
            email: 'Заполните это поле',
            theme: 'Заполните это поле',
            text: 'Заполните это поле',
            city: 'Заполните это поле',
        },
        errorElement : 'span',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          var label = $(element).parent();
          if (placement) {
            $(label).append(error)
          } else {
            error.insertBefore(label);
          }
        }
    });

    if($('.header_logout').length){

        if($(window).width() <= 1100){
            $('.content').css("padding-top",60);
        }
    }

    
    // function logoutBtn(){
        
    //     $(window).resize(function(e){
    //         if($(window).width() <= 1100){
    //             $('.nav').append($('.logout_menu_append'));
    //         } else {
    //             $('.logout__links').append($('.logout_menu_append'));
    //         }
    //     });
    // }



    
    // $("#getgift form").validate({
    //     rules: {
    //         'birthday': {
    //           required: true
    //         },
    //         'address-registration': {
    //           required: true
    //         },
    //         'address-delivery': {
    //           required: true
    //         },
    //         'city': {
    //           required: true
    //         },
    //         'street': {
    //           required: true
    //         },
    //         'house': {
    //           required: true
    //         },
    //         'flat': {
    //           required: true
    //         }
    //     },
    //     errorElement : 'span',
    //     errorPlacement: function(error, element) {
    //       var placement = $(element).data('error');
    //       var label = $(element).siblings('.feild__title');
    //       if (placement) {
    //         $(label).append(error)
    //       } else {
    //         error.insertAfter(label);
    //       }
    //     }
    // });


    // $(".section-main.main_sale").mCustomScrollbar({
    //     theme:'rounded'
    // });
    
};
